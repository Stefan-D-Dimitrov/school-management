import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;

public class Student{
	private String mName;
	private int mAge;
	private String mGender;
	private double mAverageGrade;
	private int mEntityId;
	
	public Student(String name, int age, String gender, int entityId) {
		mName = name;
		mAge = age;
		mGender = gender;
		mEntityId = entityId;
		
		Random number = new Random();
		mAverageGrade = 2 + (6 - 2) * number.nextDouble();
	}

	public String getName() {
		return mName;
	}

	public int getAge() {
		return mAge;
	}

	public String getGender() {
		return mGender;
	}

	public double getAverageGrade() {
		return mAverageGrade;
	}

	public int getEntityId() {
		return mEntityId;
	}
	
	public String toString() {
		NumberFormat formatter = new DecimalFormat("#0.00");
		String averageGrade = formatter.format(mAverageGrade);
		return "Name:\t" + mName +
				"\nAge:\t" + mAge + 
				"\nGender:\t" + mGender + 
				"\nGrade:\t" + averageGrade +
				"\nEntity Id: " + mEntityId + "\n\n";
	}
	
	public double calculatePayment(ArrayList<Institute> list) {
		Institute school = new School();
		
		for(Institute var : list) {
			if(var.getId() == mEntityId) {
				school = var;
			}
		}
		
		return (mAge/school.calculateAverage())*100 + school.getTax();
	}
}
