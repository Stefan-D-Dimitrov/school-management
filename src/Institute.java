import java.util.ArrayList;

public abstract class Institute {
	private int mId;
	private String mName;
	private String mAddress;
	protected int tax;
	private ArrayList<Student> studentList;
	
	public Institute() {}
	public Institute(int id, String name, String address) {
		mId = id;
		mName = name;
		mAddress = address;
		studentList = new ArrayList<>();
	}

	public int getId() {
		return mId;
	}
	public String getName() {
		return mName;
	}
	
	public int getTax() {
		return tax;
	}
	
	public ArrayList<Student> getList(){
		return studentList;
	}
	
	public void addStudent(Student student) {
		studentList.add(student);
	}

	public double calculateAverage() {
		double sum = 0;
		for (Student student : studentList) {
			sum += student.getAverageGrade();
		}
		return sum / studentList.size();
	}
	
	public Student topPerformingStudent() {
		double max = Integer.MIN_VALUE;
		Student maxStudent = studentList.get(0);
		for(Student student : studentList) {
			if(max < student.getAverageGrade()) {
				max = student.getAverageGrade();
				maxStudent = student;
			}
		}
		
		return maxStudent;
	}

	public void greet(Student student) {
		System.out.printf("Hello %s and welcome to %s%n%n", student.getName(), mName);
	}
}
