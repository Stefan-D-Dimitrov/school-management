import java.util.ArrayList;

public class SchoolManagement {
	public static void main(String args[]) {
		// Initialise ArrayList of schools
		ArrayList<Institute> schools = new ArrayList<>();
		schools.add(new School(1, "Mountainridge Institute", "3136  Harper Street"));
		schools.add(new School(2, "Riverbank High", "3215  Victoria Street"));
		schools.add(new School(3, "Faraday Technical School", "720  Lena Lane"));
		schools.add(new University(4, "Santa Clara University", "2320  Hamill Avenue"));
		schools.add(new University(5, "Bowdoin College", "4346  Charack Road"));

		ArrayList<Student> allStudents = new ArrayList<>();
		ArrayList<Student> maleStudents = new ArrayList<>();
		ArrayList<Student> femaleStudents = new ArrayList<>();
		ArrayList<Student> topPerforming = new ArrayList<>();

		double totalIncome = 0.0;
		Student maxStudent = null;
		double maxContributor = Integer.MIN_VALUE;

		for (int i = 0; i < schools.size(); i++) {
			// Generate all students for all schools
			StudentUtility.generateStudents(schools.get(i));
			// Fill list with all students
			allStudents.addAll(schools.get(i).getList());
			// Fill list with top performing students for every facility
			topPerforming.add(schools.get(i).topPerformingStudent());
			
			for (Student student : schools.get(i).getList()) {

				// Fill male and female lists
				if (allStudents.get(i).getGender().equalsIgnoreCase("male")) {
					maleStudents.add(allStudents.get(i));
				} else if (allStudents.get(i).getGender().equalsIgnoreCase("female")) {
					femaleStudents.add(allStudents.get(i));
				}

				// Calculate Top contributor
				if (maxContributor < student.calculatePayment(schools)) {
					maxStudent = student;
					maxContributor = student.calculatePayment(schools);
				}
				// Calculate total income from all facilities
				totalIncome += student.calculatePayment(schools);
			}
		}

		// Calculate average grades for each school
		System.out.println("Average grades for each facility: ");
		for (int i = 0; i < schools.size(); i++) {
			System.out.printf(schools.get(i).getName() + " %.2f%n", schools.get(i).calculateAverage());
		}

		System.out.println();

		// Displays top performing student for each school
		System.out.println("Top performing student of each school:");
		for (int i = 0; i < schools.size(); i++) {
			System.out.printf(schools.get(i).getName() + ": %n" + schools.get(i).topPerformingStudent().toString());
		}

		// Displays overall top performing student
		System.out.println("Top performing student of all: ");
		System.out.println(StudentUtility.topPerformingStudent(topPerforming).toString());
		topPerforming = null;

		// Displays performing "male" student
		System.out.println("Top performing male of all: ");
		System.out.println(StudentUtility.topPerformingStudent(maleStudents).toString());
		maleStudents = null;

		// Displays performing "female" student
		System.out.println("Top performing female of all: ");
		System.out.println(StudentUtility.topPerformingStudent(femaleStudents).toString());
		femaleStudents = null;

		// Displays total income of institutes and top contributor
		System.out.printf("Total income of all institutes: %.2f%n", totalIncome);
		System.out.println("Top contributor: " + maxStudent.getName() + " from "
				+ schools.get(maxStudent.getEntityId() - 1).getName());
	}
}
