import java.util.ArrayList;
import java.util.Random;

public class StudentUtility {
	public static void generateStudents(Institute institute) {
		Student student;
		Random rand = new Random();
		for (int i = 1; i <= 10; i++) {
			if (i % 2 == 0) {
				student = new Student("Student" + i, 10 + (rand.nextInt(8-2)+2), "male", institute.getId());
			} else {
				student = new Student("Student" + i, 10 + (rand.nextInt(8-2)+2), "female", institute.getId());
			}
			
			institute.addStudent(student);
			institute.greet(student);
		}
	}
	
	public static Student topPerformingStudent(ArrayList<Student> list) {
		double max = Integer.MIN_VALUE;
		Student maxStudent = list.get(0);
		for(Student student : list) {
			if(max < student.getAverageGrade()) {
				max = student.getAverageGrade();
				maxStudent = student;
			}
		}
	
		return maxStudent;
	}
}
